# Animated Navigation

Navigation toggling. What more than you want?

I added CSS Variables to this, since I felt this project benefitted from them.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
